﻿$(document).ready(function () {
    //Render TagCloud
    try {
        var data = JSON.parse($("#hdfTagCloudDataList").val());
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                var words = [];
                dataItem = data[i];
                chart_callback(dataItem["ChartData"], dataItem["ProjectId"], '#f7f8fa');
                for (var k = 0; k < dataItem["TrendingData"].length; k++) {
                    trendingData = dataItem["TrendingData"][k];
                    var term = trendingData["key"];
                    var weight = trendingData["score"];
                    var searchlink = "/Administrator/Register.aspx";//"/Administrator/LandingPageLogin.aspx?prjid=" + dataItem["ProjectId"];
                    var myterm = { text: term, weight: parseFloat(weight), link: searchlink };
                    words.push(myterm);
                }
                $("#tagcloudLoader_" + dataItem["ProjectId"]).css("height", 200);
                $("#tagcloudLoader_" + dataItem["ProjectId"]).css("width", $("#tagcloudLoader_" + dataItem["ProjectId"]).parent().width());
                $("#tagcloudLoader_" + dataItem["ProjectId"]).html("");
                $("#tagcloudLoader_" + dataItem["ProjectId"]).jQCloud(words, {
                    shape: 'rectangle'
                },
                    {
                        autoResize: true
                    });
            }
        }
    }
    catch (e) { }
});

function chart_callback(data, projectID) {

    $("#chart_result" + projectID).children().show();

    chart_data = data;

    mentionData = getCountPerDayData(data["charts"], "mention");
    reachData = getCountPerDayData(data["charts"], "reach");
    sentimentPositiveData = getCountPerDayData(data["charts"], "positive");
    sentimentNegativeData = getCountPerDayData(data["charts"], "negative");
    //hàm vẽ chart   
    DrawMentionsChart(mentionData, sentimentPositiveData, sentimentNegativeData, projectID);
}

function DrawMentionsChart(mentionData, sentimentPositiveData, sentimentNegativeData, projectID, backgroundColor) {
    var shortMonths = "Th1,Th2,Th3,Th4,Th5,Th6,Th7,Th8,Th9,Th10,Th11,Th12".split(",");
    Highcharts.setOptions({
        lang: {
            shortMonths: [shortMonths[0], shortMonths[1], shortMonths[2], shortMonths[3], shortMonths[4], shortMonths[5], shortMonths[6], shortMonths[7], shortMonths[8], shortMonths[9], shortMonths[10], shortMonths[11]]
        },
        global: {
            useUTC: false
        }
    });
    $("#chart_main" + projectID).children().remove();
    mentionschart = $("#chart_main" + projectID).highcharts({
        chart: {
            type: 'line',
            width: null,
            height: 250,
            spacingTop: 30,
            spacingLeft: 10,
            backgroundColor: backgroundColor
        },
        legend: {
            align: 'left',
            verticalAlign: 'top',
            x: 30,
            y: -25,
            floating: true,
        },
        title: {
            text: "",
            x: -20 //center
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                },
                dataLabels: {
                    enabled: true,
                    y: -5,
                    useHTML: true,
                    formatter: function () {
                        if (this.point.options.showLabel) {
                            return $('<div/>').css({
                                'color': '#FFF',
                                'border': this.series.color,
                                'backgroundColor': this.series.color,
                                'padding': 5,
                            }).text(this.y)[0].outerHTML;

                        }
                        return null;
                    }
                },
                animation: {
                    duration: 2000,
                    easing: 'easeOutBounce'
                },
                events: {
                    show: function () {
                        callback(this.chart);
                    }
                },
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            window.location.href = "Administrator/Register.html";//"/Administrator/LandingPageLogin.aspx?prjid=" + projectID;
                        }
                    }
                }
            }
        },
        xAxis: {
            gridLineDashStyle: 'dot',
            gridLineColor: '#E1E1E1',
            gridLineWidth: 1,
            type: 'datetime',
            labels: {
                formatter: function () {
                    return Highcharts.dateFormat('%e %b', this.value);
                }
            },
        },
        yAxis: [{
            title: {
                text: ''
            },
            lineWidth: 0,
        }, {
            title: {
                text: ''
            },
            lineWidth: 0,
            opposite: true
        }],
        tooltip: {
            borderRadius: 10,
            borderColor: '#24E6B1',
            crosshairs: true,
            shared: true,
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><td style="color: {series.color};font-size:12px;font-family: Arial, Helvetica;text-transform: uppercase;">{series.name}: </td></tr>' +
            '<tr><td style="color: {series.color};font-size:11px;font-family: Arial, Helvetica;"><small>{point.x:%Y-%m-%d %H:%M:%S}</small></td></tr>' +
            '<tr><td style="color: {series.color};font-size:20px;font-family: Arial, Helvetica;padding-top:10px;padding-bottom:10px;"><b>{point.y}</b><br></td></tr>',
            footerFormat: '</table>',
        },
        series: [{
            name: "Lượng đề cập",
            data: mentionData,
            color: '#0077CC',
            lineWidth: 3,
            marker: {
                enabled: false,
                symbol: 'circle'
            }
        }, {
            name: "Lượng tích cực",
            data: sentimentPositiveData,
            color: '#24E6B1',
            lineWidth: 3,
            marker: {
                enabled: false,
                symbol: 'circle'
            }
        }, {
            name: "Lượng tiêu cực",
            data: sentimentNegativeData,
            color: '#FF0000',
            lineWidth: 3,
            marker: {
                enabled: false,
                symbol: 'circle'
            }
        }]
    }, callback);

    function callback(chart) {
        var series = chart.series[0],
            points = series.points,
            pLen = points.length,
            i = 0,
            lastIndex = pLen - 1,
            minIndex = series.processedYData.indexOf(series.dataMin),
            maxIndex = series.processedYData.indexOf(series.dataMax);

        points[minIndex].options.showLabel = true;
        points[maxIndex].options.showLabel = true;
        //points[lastIndex].options.showLabel = true;
        series.isDirty = true;

        series = chart.series[1],
            points = series.points,
            pLen = points.length,
            i = 0,
            lastIndex = pLen - 1,
            minIndex = series.processedYData.indexOf(series.dataMin),
            maxIndex = series.processedYData.indexOf(series.dataMax);
        if (chart.series[1].visible) {
            points[minIndex].options.showLabel = true;
            points[maxIndex].options.showLabel = true;
        }
        //points[minIndex].options.showLabel = false;
        //points[maxIndex].options.showLabel = false;
        //points[lastIndex].options.showLabel = true;
        series.isDirty = true;
        chart.redraw();
    }
}

function getCountPerDayData(ydata, type) {
    yaxisdata = [];

    switch (type) {
        case "mention":
            if (ydata["chart_main"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date2").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_main"]["pages_count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);
                    yaxisdata.push(dt);
                }
            }
            break;
        case "reach":
            if (ydata["chart_main_ranges"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_main_ranges"]["count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
        case "comment":
            if (ydata["chart_comments_counts"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_comments_counts"]["count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
        case "like":
            if (ydata["chart_likes_counts"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_likes_counts"]["count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
        case "share":
            if (ydata["chart_shares_counts"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_shares_counts"]["count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
        case "positive":
            if (ydata["chart_sentiment"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_sentiment"]["sentiment_positive_count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
        case "negative":
            if (ydata["chart_sentiment"] == null) {
                diff = calculateDaysBetweenDate($("#date1").val(), $("#date2").val());
                current_date = new Date($("#date1").val());
                if (diff == 0) {
                    dt = [];
                    dt.push(Date.UTC(current_date.getFullYear(), parseInt(current_date.getMonth()) - 1, current_date.getDate(), -7, 0, 0));
                    dt.push(0);

                    yaxisdata.push(dt);
                }
                else if (diff <= 30) {
                    for (i = 0; i < diff; i += 2) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 2);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                } else {
                    for (i = 0; i < diff; i += 30) {
                        dt = [];
                        var from = new Date(current_date.getFullYear(), current_date.getMonth(), current_date.getDate() + 30);
                        current_date = from;
                        dt.push(Date.UTC(from.getFullYear(), parseInt(from.getMonth()) - 1, from.getDate()));
                        dt.push(0);

                        yaxisdata.push(dt);
                    }
                }
            } else {
                b = ydata["chart_sentiment"]["sentiment_negative_count_per_day"];
                for (i = 0; i < Object.keys(b).length; i++) {
                    day = Object.keys(b)[i];
                    dt = [];
                    dt.push(Date.parse(day + "T00:00:00.000"));
                    dt.push(b[day]);

                    yaxisdata.push(dt);
                }
            }
            break;
    }

    return yaxisdata;
}